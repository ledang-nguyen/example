export default (arrayVariable) => {
  return {
    all: false,
    integrations: [],
    getIntegrations() {
      this.integrations = window[arrayVariable];
    },
    showAll() {
      this.all = true;
    }
  }
}
