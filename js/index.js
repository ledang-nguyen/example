import 'alpinejs';
import integrations from './integrations';
import integrationsList from './integrationsList';
import TextWithMedia from './TextWithMedia';

if (!document.querySelector('.Main').classList.contains('-single') && document.querySelector('.Integrations')) {
  window.integrations = integrations;
}

if (document.querySelector('.IntegrationsList')) {
  window.integrationsList = integrationsList;
}

if (document.querySelector('.TextWithMedia')) {
  new TextWithMedia($('.TextWithMedia-right')).init();
}
