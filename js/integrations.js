const axios = require('axios');

export default () => {
  return {
    query: '',
    category: 'all',
    integrations: [],
    bgColor(color) {
      return 'background-color: ' + color;
    },
    sortedIntegrations() {
      return this.integrations.sort((x, y) => {
        return (x.top === y.top) ? 0 : x.top ? -1 : 1;
      });
    },
    queriedIntegrations() {
      return this.sortedIntegrations().filter((item) => item.title.toLowerCase().indexOf(this.query.toLowerCase()) !== -1);
    },
    request() {
      axios.get(`${wpglobals.ajax_api}/integrations/?_wpnonce=${wpglobals.ajax_api_nonce}`)
        .then((response) => {
          if (response.status === 200) {
            this.integrations = response.data.data;
          }
        })
        .catch((error) => {
          console.log(error);
        })
    }
  }
}
