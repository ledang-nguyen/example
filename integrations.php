<?php
/**
 * Integrations Component (Integrations)
 *
 * @author Le Dang Nguyen
 */
?>

<?php
$categories = get_terms([
  'taxonomy' => 'integrations-category',
]);
?>

<div class="Integrations" x-data="integrations()" x-init="request()">
  <div class="Integrations-wrapper">
    <div class="Integrations-left">
      <ul class="Integrations-categories">
        <li class="Integrations-categoriesItem -title">Categories</li>
        <li class="Integrations-categoriesItem" :class="{'-active': category === 'all'}">
          <a class="Integrations-categoriesItemLink" href="#" @click.prevent="category = 'all'">All</a>
        </li>

        <?php foreach ($categories as $category) : ?>
          <li class="Integrations-categoriesItem" :class="{'-active': category === '<?php echo esc_attr($category->slug); ?>'}">
            <a class="Integrations-categoriesItemLink" href="#" @click.prevent="category = '<?php echo esc_attr($category->slug); ?>'"><?php echo esc_html($category->name); ?></a>
          </li>
        <?php endforeach ?>
      </ul>
    </div>
    <div class="Integrations-right">
      <div class="Integrations-search">
        <label class="Integrations-searchLabel">
          <input class="Integrations-searchInput" type="text" placeholder="Search for Integrations" x-model="query">
        </label>
      </div>
      <div class="Integrations-list" :class="{'-all': category === 'all'}">
        <template x-for="item in queriedIntegrations()" :key="item.title">
          <div class="Integrations-item" :class="{'-top': item.top && category === 'all'}" x-show="item.category.indexOf(category) !== -1 || category === 'all'" :data-type="item.type">
            <div class="Integrations-itemInner">
              <div class="Integrations-itemLogo" :style="item.top && category === 'all' ? bgColor(item.color) : null">
                <img class="Integrations-itemLogoImg" :src="item.top && category === 'all' ? item.altLogo : item.logo" :alt="item.title">
              </div>
              <div class="Integrations-itemInfo">
                <h3 class="Integrations-itemTitle" x-text="item.title"></h3>
                <div class="Integrations-itemDesc" x-html="item.description"></div>
                <a class="Integrations-itemLink -card" :href="item.url"></a>
              </div>
            </div>
          </div>
        </template>
      </div>
    </div>
  </div>
</div>
