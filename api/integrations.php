<?php
/**
 * Register a rewrite endpoint for the API.
 */

add_action('init', function () {
  add_rewrite_tag('%ajax-api%', '([^&]+)');
  add_rewrite_rule('ajax-api/integrations', 'index.php?ajax-api=integrations', 'top');
});

add_action('template_redirect', function () {
  if (!isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], "ajax_api_nonce")) {
    return;
  }

  if (get_query_var('ajax-api') === 'integrations') {
    $response = [];

    function compare_posts_by_types($a, $b) {
      $types = [
        'Native' => 1,
        'Extension' => 2,
        'Third Party' => 3,
        'No-Code' => 4,
      ];

      return $types[$a['type']] > $types[$b['type']];
    }

    $integrations = new WP_Query([
      'post_type' => 'integrations',
      'posts_per_page' => -1,
    ]);

    while ($integrations->have_posts()) : $integrations->the_post();
      $categories = get_the_terms(get_the_ID(), 'integrations-category');
      $categoriesArray = [];

      foreach ($categories as $category) {
        array_push($categoriesArray, $category->slug);
      }

      array_push($response, [
          'title' => get_the_title(),
          'description' => get_the_content(),
          'url' => get_the_permalink(),
          'logo' => wp_get_attachment_image_url(get_field('logo'), 'full'),
          'altLogo' => get_field('alternate_logo'),
          'color' => get_field('color'),
          'category' => $categoriesArray,
          'type' => get_field('type'),
          'top' => get_field('top'),
      ]);
    endwhile;

    wp_reset_postdata();

    usort($response, 'compare_posts_by_types');

    wp_send_json_success($response);
  }
});
